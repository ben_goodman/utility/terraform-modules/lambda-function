# AWS Lambda Function

## Variables

| Name | Description | Required | Default |
|---|---|---|---|
| `uid` | A unique identifier. | yes | |
|`lambda_payload`| The path to the lambda function's source code. | yes | |
| `function_name` | The name of the lambda function. | no | `"default"` |
| `publish` | Whether to publish the lambda function. | no | `false` |
| `runtime` | The runtime to use for the lambda function. | no | `"nodejs18.x"` |
| `environment_variables` | A map of environment variables to set for the lambda function. | no | `null` |
| `function_handler` | The function entrypoint. | no | `"index.handler"` |
|`timeout`| The amount of time the lambda function has to run. | no | `3` |
| `memory_size` | The amount of memory to allocate to the lambda function. | no | `128` |


### Lambda Payload Structure

The `lambda_payload` variable takes an object with the following structure:

```hcl
object({
    output_path         = string
    output_base64sha256 = string
})
```

This is compatible with the output of the `archive_file` data source.

```hcl
data "archive_file" "lambda_payload_viewer_request" {
    type             = "zip"
    source_file      = "${path.module}/dist/origin_request.js"
    output_file_mode = "0666"
    output_path      = "${path.module}/dist/origin_request.js.zip"
}

module "viewer_request_handler" {
    source = "gitlab.com/ben_goodman/lambda-function/aws"
    version = "1.3.0"
    org          = var.resource_namespace
    project_name = var.project_name
    lambda_payload    = data.archive_file.lambda_payload_viewer_request
}
```

## Outputs

| Name | Description |
|---|---|
| `namespace` | The namespace of the project. |
|`name`| The name of the function. |
|`version`| The version of the function. |
|`qualified_arn`| The ARN of the function. |
|`invoke_arn`| The ARN to be used for invoking the function. |
