# AWS Lambda Function Rust Handler Example

This module creates an AWS Lambda function with a Rust handler, and a corresponding function URL.  cURL-ing the function URL will return a standard "Hello, World!" response from the Lambda function.

## Instructions

The handler must be built before running Terraform.  Install `cargo-lambda` and from `examples/rust/handler` run `make handler`.  This will compile the rust source to a lambda-compatible binary.  Once the handler is built, run the usual `init/plan/apply` from the `examples/typescript` directory.