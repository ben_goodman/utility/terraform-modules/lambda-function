data "archive_file" "lambda_payload" {
    type             = "zip"
    source_file      = "${path.module}/target/lambda/handler/bootstrap"
    output_file_mode = "0666"
    output_path      = "${path.module}/bootstrap.zip"
}

module "lambda_function" {
    source = "gitlab.com/ben_goodman/lambda-function/aws"
    version = "1.3.0"
    org              = "my-namespace"
    project_name     = "example-rust-project"
    lambda_payload   = data.archive_file.lambda_payload
    publish          = true
    runtime          = "provided.al2"
    function_handler = "bootstrap"
}

resource "aws_lambda_function_url" "example" {
  function_name      = module.lambda_function.name
  authorization_type = "NONE"
}

output "function_url" {
    value = aws_lambda_function_url.example.function_url
}