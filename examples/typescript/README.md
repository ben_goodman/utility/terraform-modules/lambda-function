# AWS Lambda Function Typescript Handler Example

This module creates an AWS Lambda function with a Typescript handler, and a corresponding function URL.  cURL-ing the function URL will return a standard "Hello, World!" response from the Lambda function.

## Instructions

The handler must be built before running Terraform.  From `examples/typescript/handler` run `make handler`.  This will transpile the Typescript source to a lambda-compatible Javascript module, saved as `dist/index.js`.  Once the handler is built, run the usual `init/plan/apply` from the `examples/typescript` directory.
