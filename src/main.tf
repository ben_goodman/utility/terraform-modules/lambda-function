resource "aws_lambda_function" "main" {
    publish          = var.publish
    function_name    = var.function_name
    runtime          = var.runtime
    handler          = var.function_handler
    filename         = var.lambda_payload.output_path
    source_code_hash = var.lambda_payload.output_base64sha256
    package_type     = "Zip"
    role             = var.role == null ? aws_iam_role.lambda_iam_role.arn : var.role.arn
    timeout          = var.timeout
    memory_size      = var.memory_size
    layers           = var.layers

    environment {
        variables = var.environment_variables
    }
}
