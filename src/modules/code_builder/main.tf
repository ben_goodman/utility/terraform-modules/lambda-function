resource "null_resource" "build_handler_function" {
    provisioner "local-exec" {
        working_dir = var.working_dir
        command     = var.command
        environment = var.environment_variables
    }
    triggers = {
        always_run = "${timestamp()}"
    }
}

data "null_data_source" "wait_for_build_handler_function" {
  inputs = {
    lambda_exporter_id = "${null_resource.build_handler_function.id}"
    source_file = var.archive_source_file
  }
}

data "archive_file" "handler_function_payload" {
    depends_on = [ data.null_data_source.wait_for_build_handler_function ]
    type             = "zip"
    source_file      = data.null_data_source.wait_for_build_handler_function.outputs.source_file
    output_file_mode = "0666"
    output_path      = "${var.archive_source_file}.zip"
}