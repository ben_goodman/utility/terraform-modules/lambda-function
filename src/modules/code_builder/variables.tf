variable "command" {
    type = string
}

variable "environment_variables" {
    type = map(string)
    default = {}
}

variable "working_dir" {
    type = string
}

variable "archive_source_file" {
    type = string
    description = "The path to the build artifact to be zipped."
}
