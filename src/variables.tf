###########################################
# Required variables
###########################################
variable "function_name" {
    type        = string
    description = "The name of the lambda function."
    default     = "default"
}

variable "lambda_payload" {
    type = object({
        output_path         = string
        output_base64sha256 = string
    })
    description = "The path to the lambda function's source code."
}



######################################
# Optional Variables
######################################
variable "publish" {
    type        = bool
    description = "Whether or not to publish changes to the lambda function/handler as an updated version."
    default     = true
}

variable "runtime" {
    type        = string
    description = "The runtime to use for the lambda function."
    default     = "nodejs20.x"
}

variable "environment_variables" {
    type        = map(string)
    description = "A map of environment variables to pass to the lambda function."
    default     = null
}

variable "function_handler" {
    type        = string
    description = "The name of the lambda function's handler."
    default     = "index.handler"
}

variable "timeout" {
    type        = number
    description = "The amount of time the lambda function has to run before timing out."
    default     = 3
}

variable "memory_size" {
    type        = number
    description = "The amount of memory to allocate to the lambda function."
    default     = 128
}

variable "role" {
    description = "The role to attach to the lambda function."
    default     = null
}

variable "layers" {
    type        = list(string)
    description = "A list of ARNs for layers to attach to the lambda function."
    default     = []
}
