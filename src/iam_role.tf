data "aws_iam_policy_document" "execution_role" {
    statement {
    effect = "Allow"

    principals {
        type        = "Service"
        identifiers = [
            "lambda.amazonaws.com",
            "edgelambda.amazonaws.com"
        ]
    }

        actions = ["sts:AssumeRole"]
    }
}


resource "aws_iam_role" "lambda_iam_role" {
    name               = "${var.function_name}.default-exec-role"
    assume_role_policy = data.aws_iam_policy_document.execution_role.json
}

resource "aws_lambda_permission" "allow_cloudfront" {
  function_name = aws_lambda_function.main.function_name
  statement_id  = "AllowExecutionFromCloudFront"
  action        = "lambda:GetFunction"
  principal     = "edgelambda.amazonaws.com"
}
